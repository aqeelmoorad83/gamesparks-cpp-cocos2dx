from install_dependencies import install_cocos, download_cocos_dependencies
import os
import shutil
import subprocess
import platform

if __name__ == '__main__':
	#compile_base_sdk()
	install_cocos()
	download_cocos_dependencies()

	current_wd = os.getcwd()

	if platform.system() == 'Darwin':
		BUILD_ROOT = os.path.abspath(os.path.join( __file__, '..', 'GameSparksSample', 'proj.ios_mac' ))
		os.chdir(BUILD_ROOT)
		subprocess.check_call([
			'xcodebuild',
			'-target',
			"GameSparksSample-desktop"
		])
	else:
		print('compiling sample via this script not supported on %s, please build the project manually' % platform.system())
